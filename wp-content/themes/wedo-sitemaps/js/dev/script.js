var $ = jQuery.noConflict();
$(document).ready(function() {

	var footerHeight = $('footer').outerHeight();
    $('.footer-fix').css('padding-bottom', footerHeight);

    $('.notes-trigger').click(function(e) {
        e.preventDefault();
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).next('.notes-content').slideUp();
        } else {
            $('.notes-trigger.active').removeClass('active');
            $('.notes-content').slideUp();
            $(this).addClass('active');
            $(this).next('.notes-content').slideDown();
        }
    });

});

enquire
    .register("screen and (min-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    }, true)
    .register("screen and (max-width:31em)", function() { 
        $(document).ready(function() {
            
        });
    });

var ieVersion = null;
if (document.body.style['msFlexOrder'] != undefined) {
    ieVersion = "ie10";
    $('html').addClass('ie-10');
}
if (document.body.style['msTextCombineHorizontal'] != undefined) {
    ieVersion = "ie11";
    $('html').removeClass('ie-10').addClass('ie-11');
}
