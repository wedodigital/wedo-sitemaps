<?php get_header(); ?>

	<?php if (have_posts()) { ?>
		<?php while ( have_posts() ) { ?>
			<?php the_post(); ?>
			<section class="header-grad">
				<svg class="landing-flood">
					<defs>
						<linearGradient id="greenGradient">
				            <stop offset="0" stop-color="#a8e063">
				            	<animate attributeName="stop-color" values="#a8e063;#f83600;#49a09d;#6a3093;#f46b45;#34e89e;#f4c4f3;#ee0979;#a8e063;" dur="30s" repeatCount="indefinite" />
				            </stop>
				            <stop offset="100%" stop-color="#56ab2f">
				            	<animate attributeName="stop-color" values="#56ab2f;#fe8c00;#5f2c82;#a044ff;#eea849;#0f3443;#fc67fa;#ff6a00;#56ab2f;" dur="30s" repeatCount="indefinite" />
				            </stop>
				        </linearGradient>
					</defs>
					<rect width="100%" height="100%" fill= "url(#greenGradient)" />
				</svg>
			</section>
			<section class="sitemap-overlay">
				<article class="sitemap-article">
					<h1 class="page-title"><?php the_title(); ?></h1>
					<section class="sitemap">
						<span class="primary">Home</span>

						<?php if(get_field('sitemap')) { ?>
							<ul class="parents">
								<?php while(the_repeater_field('sitemap')) { ?>
									<?php $count++; ?>
									<li>
										<span><?php the_sub_field('parent_page'); ?></span>
										<?php if(get_sub_field('child_pages')) { ?>
											<ul>
												<?php while(the_repeater_field('child_pages')) { ?>
													<li>
														<span><?php the_sub_field('child_page'); ?></span>
														<?php if(get_sub_field('tertiary_pages')) { ?>
															<ul>
																<?php while(the_repeater_field('tertiary_pages')) { ?>
																	<li><span><?php the_sub_field('tertiary_page'); ?></span></li>
																<?php } ?>
															</ul>
														<?php } ?>
													</li>
												<?php } ?>
											</ul>
										<?php } ?>
									</li>
								<?php } ?>
							</ul>
						<?php } ?>

						<?php if(get_field('homepage_notes') || get_field('sitewide_notes') || get_field('header_notes') || get_field('footer_notes')) { ?>
							<section class="notes">
								<?php if(get_field('homepage_notes')) { ?>
									<a href="#" class="notes-trigger">Homepage Notes</a>
									<div class="notes-content">
										<?php the_field('homepage_notes'); ?>
									</div>
								<?php } ?>
								<?php if(get_field('sitewide_notes')) { ?>
									<a href="#" class="notes-trigger">Site Notes</a>
									<div class="notes-content">
										<?php the_field('sitewide_notes'); ?>
									</div>
								<?php } ?>
								<?php if(get_field('header_notes')) { ?>
									<a href="#" class="notes-trigger">Header Notes</a>
									<div class="notes-content">
										<?php the_field('header_notes'); ?>
									</div>
								<?php } ?>
								<?php if(get_field('footer_notes')) { ?>
									<a href="#" class="notes-trigger">Footer Notes</a>
									<div class="notes-content">
										<?php the_field('footer_notes'); ?>
									</div>
								<?php } ?>
							</section>
						<?php } ?>

					</section>
					<div class="footer-w"></div>
				</article>
			</section>
		<?php } ?>
	<?php } ?>

<?php get_footer(); ?>
